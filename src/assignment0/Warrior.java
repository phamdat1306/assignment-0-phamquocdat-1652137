package assignment0;

public class Warrior {
	private int baseHP;
	private int wp;
	public Warrior(int baseHP, int wp) {
		super();
		this.baseHP = baseHP;
		this.wp = wp;
	}
	public int getBaseHP() {
		return baseHP;
	}
	public int getWp() {
		return wp;
	}
	public int getRealHP(){
		if(wp==0){
			return baseHP/10;
		}
		else if(wp==1){
			return baseHP;
		}
		return 0;
	}
}
